
To run Thar cmake version 3 or higher is needed!

```bash
cd Thar
cmake3 .
make
```

More information about the setup needed for the RD53A MUX measurements can be found https://atlaswiki.lbl.gov/pixels/rd53a/mux 
