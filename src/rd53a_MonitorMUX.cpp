#include <iostream>
#include <chrono>
#include <unistd.h>
#include "SpecController.h"
#include "Rd53a.h"

#define EN_RX2 0x1
#define EN_RX1 0x2
#define EN_RX4 0x4
#define EN_RX3 0x8
#define EN_RX6 0x10
#define EN_RX5 0x20
#define EN_RX8 0x40
#define EN_RX7 0x80

#define EN_RX10 0x100
#define EN_RX9 0x200
#define EN_RX12 0x400
#define EN_RX11 0x800
#define EN_RX14 0x1000
#define EN_RX13 0x2000
#define EN_RX16 0x4000
#define EN_RX15 0x8000

#define EN_RX18 0x10000
#define EN_RX17 0x20000
#define EN_RX20 0x40000
#define EN_RX19 0x80000
#define EN_RX22 0x100000
#define EN_RX21 0x200000
#define EN_RX24 0x400000
#define EN_RX23 0x800000

#include <fstream>
#include <iostream>
#include <ctime>
#include <sys/stat.h>
#include "Logger.h"
#include "Keithley24XX.h"
#include "json.hpp"

loglevel_e loglevel = logINFO;

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

int main(int argc, char*argv[]) {

    SpecController spec;
    spec.init(0);
    spec.writeSingle(0x6<<14 | 0x0, 0x080000);
    spec.writeSingle(0x6<<14 | 0x1, 0xF);
    spec.setCmdEnable(0x1);
    spec.setRxEnable(0x0);

    Rd53a fe(&spec);
    //fe.setChipId(0);
    //std::cout << ">>> Configuring chip with default config ..." << std::endl;
    //fe.configure();
    //std::cout << " ... done." << std::endl;
    //std::this_thread::sleep_for(std::chrono::milliseconds(1));

    spec.setRxEnable(0x1);

    //sleep(0.01);
    //fe.writeRegister(&Rd53a::EnCoreColSync, 0);
    //fe.writeRegister(&Rd53a::EnCoreColLin1, 0);
    //fe.writeRegister(&Rd53a::EnCoreColDiff1, 0);
    //return 0;
    //sleep(0.01);
 
    //Read config file
    std::cout << ">>> Configuring chip with current config ..." << std::endl;
    std::fstream cfgFile;
    cfgFile.open(argv[4], std::ios::in);
    json j;
    cfgFile >> j;
    fe.fromFileJson(j);

    std::fstream outputFile;
    std::string fileName, scanName, chipName(argv[3]);
    std::time_t t = std::time(0);
    //const int dir_err = mkdir("MUX", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    chipName = "./MUX/" + std::to_string(t) + "_" + chipName;
    std::stringstream add(argv[2]);
    unsigned int temp; add >> temp;
    
    Keithley24XX meter(argv[1], temp);
    meter.init();
    meter.setSource(KeithleyMode::CURRENT, 1e-6, 0);//1e-3, -10e-6
    meter.setSense(KeithleyMode::VOLTAGE, 2.0, 2.0);
    meter.turnOn();

    float measured_value;
    fe.writeRegister(&Rd53a::MonitorEnable, 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

//Scan all Voltage MUX
    scanName = "MonitorVmonMux";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    for (unsigned i=0; i<35; i++) {
	fe.writeRegister(&Rd53a::MonitorVmonMux, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
	measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
	outputFile << i << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();

//Scan InjVcalMed
    scanName = "InjVcalMed_Left";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<41; i++) {
        fe.writeRegister(&Rd53a::InjVcalMed, i*100.);
	 std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();
    scanName = "InjVcalMed_Right";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 12);
     std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<41; i++) {
        fe.writeRegister(&Rd53a::InjVcalMed, i*100.);
	 std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();
//InjVcalHigh
    scanName = "InjVcalHigh_Left";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 2);
     std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<41; i++) {
        fe.writeRegister(&Rd53a::InjVcalHigh, i*100.);
	 std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();

    scanName = "InjVcalHigh_Right";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 13);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<41; i++) {
        fe.writeRegister(&Rd53a::InjVcalHigh, i*100.);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();

//LinVth
    scanName = "LinVth";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 17);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<11; i++) {
        fe.writeRegister(&Rd53a::LinVth, i*100.);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();

//SyncVth
    scanName = "SyncVth";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 18);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<11; i++) {
        fe.writeRegister(&Rd53a::SyncVth, i*100.);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();

//DiffVth1
    scanName = "DiffVth1";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 21);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<11; i++) {
        fe.writeRegister(&Rd53a::DiffVth1, i*100.);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();
//DiffVth2
    scanName = "DiffVth2";
    fileName = chipName + "_VoltMonitor_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 22);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<11; i++) {
        fe.writeRegister(&Rd53a::DiffVth2, i*100.);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1000. << "\n";
    }
    outputFile.close();

//Scan IMUX
    scanName = "MonitorImonMux";
    fileName = chipName + "_CurrentMonitorVmV_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 11);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<27; i++) {
        fe.writeRegister(&Rd53a::MonitorImonMux, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i << " "<< measured_value*1e3 << "\n";
    }
    outputFile.close();

    scanName = "SyncIbiasKrum";
    fileName = chipName + "_CurrentMonitorVmV_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 11);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    fe.writeRegister(&Rd53a::MonitorImonMux, 6);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<11; i++) {
        fe.writeRegister(&Rd53a::SyncIbiasKrum, i*50);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*50. << " "<< measured_value*1e3 << "\n";
    }
    outputFile.close();

    scanName = "LinPaInBias";
    fileName = chipName + "_CurrentMonitorVmV_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 11);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    fe.writeRegister(&Rd53a::MonitorImonMux, 11);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<11; i++) {
        fe.writeRegister(&Rd53a::LinPaInBias, i*50);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*50. << " "<< measured_value*1e3 << "\n";
    }
    outputFile.close();

    scanName = "DiffPrecomp";
    fileName = chipName + "_CurrentMonitorVmV_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 11);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    fe.writeRegister(&Rd53a::MonitorImonMux, 13);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<11; i++) {
        fe.writeRegister(&Rd53a::DiffPrecomp, i*100);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1e3 << "\n";
    }

    scanName = "DiffVff";
    fileName = chipName + "_CurrentMonitorVmV_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app
    fe.writeRegister(&Rd53a::MonitorVmonMux, 11);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    fe.writeRegister(&Rd53a::MonitorImonMux, 17);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    for (unsigned i=0; i<11; i++) {
        fe.writeRegister(&Rd53a::DiffVff, i*100);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        measured_value = std::stof(meter.sense(KeithleyMode::VOLTAGE));
        outputFile << i*100. << " "<< measured_value*1e3 << "\n";
    }
    outputFile.close();

    meter.turnOff();
 
    std::cout<<"Done!"<<std::endl;
    spec.setRxEnable(0x0);
    return 0;
}
