#include <iostream>
#include <chrono>
#include <unistd.h>
#include "SpecController.h"
#include "Rd53a.h"

#define EN_RX2 0x1
#define EN_RX1 0x2
#define EN_RX4 0x4
#define EN_RX3 0x8
#define EN_RX6 0x10
#define EN_RX5 0x20
#define EN_RX8 0x40
#define EN_RX7 0x80

#define EN_RX10 0x100
#define EN_RX9 0x200
#define EN_RX12 0x400
#define EN_RX11 0x800
#define EN_RX14 0x1000
#define EN_RX13 0x2000
#define EN_RX16 0x4000
#define EN_RX15 0x8000

#define EN_RX18 0x10000
#define EN_RX17 0x20000
#define EN_RX20 0x40000
#define EN_RX19 0x80000
#define EN_RX22 0x100000
#define EN_RX21 0x200000
#define EN_RX24 0x400000
#define EN_RX23 0x800000

#include <fstream>
#include <iostream>
#include <ctime>

#include "Logger.h"
#include "Keithley24XX.h"


loglevel_e loglevel = logINFO;

//using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

int main(int argc, char*argv[]) {

    SpecController spec;
    spec.init(0);
    spec.writeSingle(0x6<<14 | 0x0, 0x080000);
    spec.writeSingle(0x6<<14 | 0x1, 0xF);
    spec.setCmdEnable(0x1);
    spec.setRxEnable(0x0);

    Rd53a fe(&spec);
    fe.setChipId(0);
    std::cout << ">>> Configuring chip with default config ..." << std::endl;
    fe.configure();
    std::cout << " ... done." << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    spec.setRxEnable(0x1);

    sleep(0.01);
    fe.writeRegister(&Rd53a::EnCoreColSync, 0);
    fe.writeRegister(&Rd53a::EnCoreColLin1, 0);
    fe.writeRegister(&Rd53a::EnCoreColDiff1, 0);
    sleep(0.01);
 
    std::fstream outputFile;
    std::string fileName, scanName, chipName(argv[3]);
    std::time_t t = std::time(0);
    chipName = std::to_string(t) + "_" + chipName;
    std::stringstream add(argv[2]);
    unsigned int temp; add >> temp;
    
    Keithley24XX meter(argv[1], temp);
   // meter.init();
   // meter.setSource(KeithleyMode::VOLTAGE, 0, 0);//1e-3, -10e-6
   // meter.setSense(KeithleyMode::CURRENT, 10e-6, 10e-6);
    //meter.turnOn();

    float measured_value;

//Read the currnet
    scanName = "MonitorIref";
    fileName = chipName + "_" + scanName + ".dat";
    outputFile.open(fileName, std::fstream::out | std::fstream::trunc);//app

    measured_value = std::stof(meter.sense(KeithleyMode::CURRENT));
    std::cout<<measured_value*1e6<<std::endl;
    outputFile <<" Iref#muA "<< measured_value*1e6 << "\n";

    outputFile.close();

    //meter.turnOff();
 

    spec.setRxEnable(0x0);
    return 0;
}
